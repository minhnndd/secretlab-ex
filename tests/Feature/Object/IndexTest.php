<?php

namespace Tests\Feature\Object;

use App\Models\SecretlabObject;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function testIndex()
    {
        SecretlabObject::factory()->count(3)->create();

        $this->get('api/v1/object/get_all_records')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'key',
                        'value',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                ],
            ]);
    }
}

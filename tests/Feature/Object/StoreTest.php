<?php

namespace Tests\Feature\Object;

use App\Models\SecretlabObject;
use Tests\TestCase;

class StoreTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequiresValueAndKey()
    {
        $this->json('POST', 'api/v1/object')
            ->assertStatus(422)
            ->assertJsonFragment([
                'key'   => ['The key field is required.'],
                'value' => ['The value field is required.'],
            ]);
    }

    public function testStoreNewSuccessfully()
    {
        $this->json('POST', 'api/v1/object', ['key' => 'key', 'value' => 'value1'])
            ->assertStatus(202)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'key',
                    'value',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $this->assertDatabaseHas('secretlab_objects', [
            'key'   => 'key',
            'value' => 'value1',
        ]);
    }

    public function testStoreExistingSuccessfully()
    {
        $object = SecretlabObject::factory(['key' => 'key'])->create();

        $this->json('POST', 'api/v1/object', ['key' => 'key', 'value' => 'value1'])
            ->assertStatus(202)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'key',
                    'value',
                    'deleted_at',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $this->assertDatabaseHas('secretlab_object_revisions', [
            'secretlab_object_id' => $object->id,
        ]);

        $this->assertDatabaseHas('secretlab_objects', [
            'key'   => 'key',
            'value' => 'value1',
        ]);
    }
}

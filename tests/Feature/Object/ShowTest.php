<?php

namespace Tests\Feature\Object;

use App\Models\SecretlabObject;
use Carbon\Carbon;
use Tests\TestCase;

class ShowTest extends TestCase
{
    public function testRequiredKey()
    {
        SecretlabObject::factory(['key' => 'key'])->create();

        $this->json('GET', 'api/v1/object/key')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'key',
                    'value',
                    'deleted_at',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    public function testNotFoundKey()
    {
        $this->json('GET', 'api/v1/object/key')
            ->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testTimestamp()
    {
        SecretlabObject::factory(['key' => 'key'])->create();
        $timestamp = Carbon::now('UTC');

        $this->call('GET', 'api/v1/object/key', ['timestamp' => $timestamp->getTimestamp()])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'value',
                ],
            ]);
    }
}

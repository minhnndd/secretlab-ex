<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ObjectResource",
 *     description="SecretlabObject resource",
 *     @OA\Xml(
 *         name="ObjectResource"
 *     )
 * )
 */
class ObjectResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\SecretlabObject[]
     */
    private $data;
}

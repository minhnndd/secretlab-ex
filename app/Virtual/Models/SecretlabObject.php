<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="SecretlabObject",
 *     description="SecretlabObject model",
 *     @OA\Xml(
 *         name="SecretlabObject"
 *     )
 * )
 */
class SecretlabObject
{

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="key",
     *      description="Key of the new object",
     *      example="A nice object"
     * )
     *
     * @var string
     */
    public $key;

    /**
     * @OA\Property(
     *      title="Value",
     *      description="Value of the new object",
     *      example="This is new object's value"
     * )
     *
     * @var string
     */
    public $value;

    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="Updated at",
     *     description="Updated at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @OA\Property(
     *     title="Deleted at",
     *     description="Deleted at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $deleted_at;
}

<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Store SecretlabObject request",
 *      description="Store SecretlabObject request body data",
 *      type="object",
 *      required={"key","value"}
 * )
 */
class StoreObjectRequest
{
    /**
     * @OA\Property(
     *      title="key",
     *      description="Name of the new object",
     *      example="a-object"
     * )
     *
     * @var string
     */
    public $key;

    /**
     * @OA\Property(
     *      title="value",
     *      description="Value of the new object",
     *      example="This is new object's value"
     * )
     *
     * @var string
     */
    public $value;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SecretlabObject
 *
 * @package App\Models
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class SecretlabObject extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'key',
        'value',
    ];

    protected $revisionable = [
        'value',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'key';
    }

    public function revisions()
    {
        return $this->hasMany(SecretlabObjectRevision::class);
    }

    public function getRevisionableAttributes()
    {
        $allowed = $this->revisionable;

        return array_filter($this->attributes, function ($key) use ($allowed) {
            return in_array($key, $allowed, true);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function getRevisionByTimestamp($timestamp)
    {
        $revision = $this->revisions()
            ->where('created_at', '<=', $timestamp)
            ->orderBy('created_at', 'desc')
            ->first();

        if (!$revision) {
            return false;
        }

        return $this->getRevisionObject($revision);
    }

    protected function getRevisionObject(SecretlabObjectRevision $revision)
    {
        $object       = $this->newModelInstance();
        $revisionData = $revision->getAttribute('data');
        foreach ($this->revisionable as $attribute) {
            if (!array_key_exists($attribute, $revisionData)) {
                continue;
            }
            $object->setAttribute($attribute, $revisionData[$attribute]);
        }

        return $object;
    }

    protected function finishSave(array $options)
    {
        parent::finishSave($options);

        $this->revisions()->create(['data' => $this->getRevisionableAttributes()]);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SecretlabObjectRevision extends Model
{
    use HasFactory;

    protected $fillable = [
        'secretlab_object_id',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function base()
    {
        return $this->belongsTo(SecretlabObject::class);
    }

}

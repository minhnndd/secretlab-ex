<?php

namespace App\Http\Controllers\Api;

class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Secretlab Object OpenApi Documentation",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="API Server"
     * )
     *
     * @OA\Tag(
     *     name="Objects",
     *     description="API Endpoints of Objects"
     * )
     */
}

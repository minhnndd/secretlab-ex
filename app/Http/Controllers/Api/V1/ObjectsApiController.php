<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreObjectRequest;
use App\Http\Resources\ObjectResource;
use App\Models\SecretlabObject;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ObjectsApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/object/get_all_records",
     *      operationId="getObjectsList",
     *      tags={"Objects"},
     *      summary="Get list of objects",
     *      description="Returns list of objects",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ObjectResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function index()
    {
        return new ObjectResource(SecretlabObject::all());
    }

    /**
     * @OA\Post(
     *      path="/object",
     *      operationId="storeObject",
     *      tags={"Objects"},
     *      summary="Store new or update existing object",
     *      description="Returns object data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreObjectRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/SecretlabObject")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     * @param \App\Http\Requests\StoreObjectRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|SecretlabObject
     */
    public function store(StoreObjectRequest $request)
    {
        $object = SecretlabObject::firstOrNew(['key' => $request->get('key')]);

        $object->fill($request->all())->save();

        return (new ObjectResource($object))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Get(
     *      path="/object/{key}",
     *      operationId="getObjectByKey",
     *      tags={"Objects"},
     *      summary="Get object information",
     *      description="Returns object data",
     *      @OA\Parameter(
     *          name="key",
     *          description="SecretlabObject key",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="timestamp",
     *          description="Timestamp",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/SecretlabObject")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      )
     * )
     * @param \App\Models\SecretlabObject $object
     *
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function show(SecretlabObject $object)
    {
        $timestamp = Request::query('timestamp');
        if ($timestamp) {
            $timestamp = Carbon::createFromTimestamp($timestamp);
            $object    = $object->getRevisionByTimestamp($timestamp);
            if ($object === false) {
                throw new NotFoundHttpException("Unable to find object with queried timestamp");
            }
        }

        return new ObjectResource($object);
    }

}

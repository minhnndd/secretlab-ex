<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectsRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secretlab_object_revisions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('secretlab_object_id')->constrained();
            $table->longText('data');
            $table->timestamps();

            $table->index(['secretlab_object_id']);
            $table->index(['created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secretlab_object_revisions');
    }
}
